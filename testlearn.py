from neuralNet import *
import os
import json
import uuid
import difflib

NET_PATH = "net"
chars = ['', '0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z', ' ', '?']

def headNet():
    if os.path.exists("head"):
        with open("head") as f:
            jo = json.load(file.read())

            num_inputs = jo['num_inputs']
            num_hidden = jo['num_inputs']
            num_outputs = jo['num_outputs']
            hidden_layer_weights = jo['hidden_layer_weights']
            hidden_layer_bias = jo['hidden_layer_bias']
            output_layer_weights = jo['output_layer_weights']
            output_layer_bias = jo['output_layer_bias']

            nn = NeuralNetwork(num_inputs, num_hidden, num_outputs, hidden_layer_weights, hidden_layer_weights, output_layer_weights, output_layer_bias)
            return nn
    else:
        nn = NeuralNetwork(256, 5, 256)

        data = {}
        data['num_inputs'] = nn.num_inputs
        data['num_hidden'] = nn.num_hidden
        data['num_outputs'] = nn.num_outputs
        data['hidden_layer_weights'] = nn.hidden_layer.dump()
        data['hidden_layer_bias'] = nn.hidden_layer.bias
        data['output_layer_weights'] = nn.output_layer.dump()
        data['output_layer_bias'] = nn.output_layer.bias

        with open("head", 'w') as f:
            f.write(str(json.dumps(data)))

        return nn


def readNet(question):
    only_files = [f for f in os.listdir(NET_PATH) if os.path.isfile(os.path.join(NET_PATH, f))]
    charedQ = [chars.index(question[i]) / len(chars) for i in range(len(question))]
    got = []

    found = False

    for fn in only_files:
        with open(NET_PATH + '/' + fn, 'r') as f:
            jo = json.loads(f.read())

            num_inputs = jo['num_inputs']
            num_hidden = jo['num_inputs']
            num_outputs = jo['num_outputs']
            hidden_layer_weights = jo['hidden_layer_weights']
            hidden_layer_bias = jo['hidden_layer_bias']
            output_layer_weights = jo['output_layer_weights']
            output_layer_bias = jo['output_layer_bias']

            nn = NeuralNetwork(num_inputs, num_hidden, num_outputs, hidden_layer_weights, hidden_layer_bias, output_layer_weights, output_layer_bias)

            chared = [chars.index(fn[i]) / len(chars) for i in range(len(fn))]
            think = ''.join([chars[int(round(n * len(chars), 1))] for n in nn.feed_forward(chared)])

            #thinks_data.append([nn, chared, think])
            got.append([nn, fn, think])

    else:
        closest = difflib.get_close_matches(question, [str(g[2]) for g in got])
        if len(closest) == 0:
            #print([str(g[2]) for g in got])
            answer = input("sys: answer for that: ")
            charedA = [chars.index(answer[i]) / len(chars) for i in range(len(answer))]

            nn = NeuralNetwork(len(charedA), 5, len(charedQ))
        else:
            print("closest: " + str(closest))
            g = [g for g in got if g[2] == closest[0]][0]

            nn = g[0]
            answer = g[1]

            charedA = [chars.index(answer[i]) / len(chars) for i in range(len(answer))]
            print("say: " + answer)


            #if input("not even close ? (n) : ").upper() == "N":

        got_right_counter = 0
        while True:
            nn.train(charedA, charedQ)
            think = ''.join([chars[int(round(n * len(chars), 1))] for n in nn.feed_forward(charedA)])

            if len(difflib.get_close_matches(question, [think])) > 0:
                got_right_counter += 1
                if got_right_counter >= 100:
                    print("got right: " + think)
                    break



        data = {}
        data['num_inputs'] = nn.num_inputs
        data['num_hidden'] = nn.num_hidden
        data['num_outputs'] = nn.num_outputs
        data['hidden_layer_weights'] = nn.hidden_layer.dump()
        data['hidden_layer_bias'] = nn.hidden_layer.bias
        data['output_layer_weights'] = nn.output_layer.dump()
        data['output_layer_bias'] = nn.output_layer.bias

        with open(NET_PATH + '/' + answer, 'w') as f:
            f.write(str(json.dumps(data)))

while True:
    question = input("? : ")
    readNet(question)
